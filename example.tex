%% MIT License
%% 
%% Copyright (c) 2018 Xavier Poczekajlo
%% 
%% Permission is hereby granted, free of charge, to any person obtaining a copy
%% of this software and associated documentation files (the "Software"), to deal
%% in the Software without restriction, including without limitation the rights
%% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%% copies of the Software, and to permit persons to whom the Software is
%% furnished to do so, subject to the following conditions:
%% 
%% The above copyright notice and this permission notice shall be included in all
%% copies or substantial portions of the Software.
%% 
%% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%% SOFTWARE.


\documentclass{article}

\usepackage{subcaption}
\usepackage{./rtgantt}
\usepackage{listings}

\title{rtgantt: real-time chronograph made easy}
\author{Xavier Poczekajlo\\Xavier.Poczekajlo@ulb.ac.be}

\begin{document}

\maketitle

\tableofcontents

\section{Introduction}
This package allows you to create very simple chronographs for real-time scheduling
with only the minimum of high-level commands. 


\textbf{Disclaimer: this package uses previous libraries for which no license
was found. If you're the author of those libraries and want to be credited, 
please send me an email at Xavier.Poczekajlo@ulb.ac.be.}

\section{Usage}
\subsection{Basic command}
The following command defines the  environment
%\begin{gantt}{
%\end{gantt}
which will contain a chronograph.
It's arguments are the following:
\newline
\texttt{$\backslash$begin\{gantt\}\{<t>\}\{<num\_of\_activity>\}\{<xscale>\}\{<yscale>}
\begin{itemize}
    \item $s<$t$>$ is the number of time instants in the chronograph.
    \item $<$num\_of\_activity$>$ is the number of lines that will be used in the chronograph.
    \item $<$xscale$>$ and $<$yscale$>$ may be used to adjust the horizontal and vertical scales respectively.
    \item Note: $<$num\_of\_activity$>$ must be equals to the number of $<$activityrow$>$ created later.
\end{itemize}
The chronograph must be described line by line. 
\newline
The command \texttt{$\backslash$activityrow\{<value>\}}
terminates the previous line and create a new one, with the name $<$value$>$. 
\newline
The command \texttt{$\backslash$activity\{<value>\}\{<length>\}} creates a new
activity starting at current line's end and lasting for $<$length$>$ units of time. It
will be named $<$value$>$ and represented by a gray box.
\newline
The command \texttt{$\backslash$lateactivity\{<value\}\{<length\}} is similar,
but using a red box to illustrate the lateness.
\newline
The command \texttt{$\backslash$lagtime\{<value>\}\{<length\}} display a white 
square with the value $<$value$>$.

The next example demonstrates the use of these commands. 
The result is displayed in Fig.~\ref{basic}.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \activityrow{$\pi_1$}
    \activity{$\tau_1$}{2} 
    \lateactivity{$\tau_3$}{1} 

    \activityrow{$\pi_2$} 
    \activity{$\tau_2$}{2} 
    \lagtime{idle}{1}
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
        \activityrow{$\pi_1$}
        \activity{$\tau_1$}{2} 
        \lateactivity{$\tau_3$}{1} 
    
        \activityrow{$\pi_2$} 
        \activity{$\tau_2$}{2} 
        \lagtime{idle}{1}
    \end{gantt}
    \end{subfigure}
    \caption{Basic usage}
    \label{basic}
\end{figure*}

The chronograph can be easily stretched using the arguments $<$xscale$>$ and $<$yscale$>$, 
as shown in Fig.~\ref{stretched}.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{0.5}{2}
    \activityrow{$\pi_1$}
    \activity{$\tau_1$}{2} 
    \lateactivity{$\tau_3$}{1} 

    \activityrow{$\pi_2$} 
    \activity{$\tau_2$}{2} 
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
    \begin{gantt}{4}{2}{0.5}{2}
        \activityrow{$\pi_1$}
        \activity{$\tau_1$}{2} 
        \lateactivity{$\tau_3$}{1} 
    
        \activityrow{$\pi_2$} 
        \activity{$\tau_2$}{2} 
    \end{gantt}
    \end{subfigure}
    \caption{Stretched chronograph}
    \label{stretched}
\end{figure*}

\subsection{Job arrivals and deadlines commands}
\texttt{rtgantt} also offers the possibility to show
job arrivals, job deadlines and to display jobs that have missed their deadlines.
\newline
The command \texttt{$\backslash$grequest\{<label>\}} draw request arrows with 
the label $<$label$>$ above. It must be tiny if something is above.
\newline
The command \texttt{$\backslash$gdeadline\{<label>\}} shows the deadline 
of the tasks contained in $<$label$>$.
\newline
The command \texttt{$\backslash$gfailure\{<label>\}} is similar, but drawn in 
red to suggest that the deadline is missed. 

The next example demonstrates the use of these two new commands, the result
is depicted in Fig.~\ref{arrival}.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{3}{2}{1}{1}
    \activityrow{$\pi_1$}
    \grequest{$\tau_1,\tau_2, 
        \tau_3$}
    \activity{$\tau_1$}{2} 
    \gfailure{$D_3$}
    \lateactivity{$\tau_3$}{1}
	\gdeadline{$D_1,D_2$} 
    
    \activityrow{$\pi_2$} 
    \activity{$\tau_2$}{2} 
    \lagtime{idle}{2}
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{3}{2}{0.8}{0.8}
        \activityrow{$\pi_1$}
        \grequest{$\tau_1,\tau_2, \tau_3$}
        \activity{$\tau_1$}{2} 
        \gfailure{$D_3$}
        \lateactivity{$\tau_3$}{1}
    	\gdeadline{$D_1,D_2$} 
        
        \activityrow{$\pi_2$} 
        \activity{$\tau_2$}{2} 
        \lagtime{idle}{2}
    \end{gantt}
    \end{subfigure}
    \caption{Job arrivals and deadlines management}
    \label{arrival}
\end{figure*}

However, the previous commands are flawed by design: 
they may be erased by the next activity.
In the last graph, this is the case for the deadline $D_3$.
It is possible to solve this problem by using absolute values instead of relative. 
Those commands are a bit more cumbersome, but their display is perfect.
The equivalent of 
\texttt{grequest, gdeadline and gfailure} are 
\texttt{grequestabs, gdeadlineabs and gfailureabs}.
All are used in the same way:\linebreak 
\texttt{$\backslash$gfailureabs[<row>]\{<value>\}\{<t>\}}, where
$<$row$>$ is optional and indicates the row where to place the arrow, 
$<$value$>$ is the value to be displayed and $<$t$>$ is the instant where
the arrow will be placed.

The use of those commands is illustrated in Fig.~\ref{absolute}.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \activityrow{$\pi_1$}
    \activity{$\tau_1$}{2} 
    \lateactivity{$\tau_3$}{1}
    
    \activityrow{$\pi_2$} 
    \activity{$\tau_2$}{2} 
    \lagtime{idle}{2}

    \grequestabs{$\tau_1,\tau_2,
        \tau_3$}{0}
    \gfailureabs[0]{$D_3$}{2}
    \gdeadlineabs{$D_1,D_2$}{3}
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
        \activityrow{$\pi_1$}
        \activity{$\tau_1$}{2} 
        \lateactivity{$\tau_3$}{1}
        
        \activityrow{$\pi_2$} 
        \activity{$\tau_2$}{2} 
        \lagtime{idle}{2}
    
        \grequestabs{$\tau_1,\tau_2, \tau_3$}{0}
        \gfailureabs[0]{$D_3$}{2}
        \gdeadlineabs{$D_1,D_2$}{3}
    \end{gantt}
    \end{subfigure}
    \caption{Usage of absolute positioning commands}
    \label{absolute}
    \label{gtask}
\end{figure*}

\subsection{Comments and global events commands}

To annotate the chronograph, it is possible to use the following commands: 
\newline
The command \texttt{$\backslash$gevent\{<label>\}\{<t>\}} prints an event
marked by a black vertical line at instant $<$t$>$ with the label $<$label$>$.
\newline
The command 
\texttt{$\backslash$gcomment\{<row>\}\{<t>\}\{<length>\}\{<label>\}\{<xshift>\}} 
adds a comment at row $<$row$>$ from $<$t$>$ to $<$t$>$ + $<$length$>$ with 
the label $<$label$>$. It is represented by an horizontal embrace.
The label may be shifted using the parameter $<$xshift$>$.

Those commands may be used at the end of the gantt graph, and must not be used 
during an activity.
Fig.~\ref{comment} demonstrates the use of these two new commands.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \activityrow{$\pi_1$}
    \activity{$\tau_1$}{2} 
    \lateactivity{$\tau_3$}{1} 

    \activityrow{$\pi_2$} 
    \activity{$\tau_2$}{2} 
    \gevent{MCR}{2}
    \gcomment{0}{0}{2}{
        \small{highest}}{0}
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
        \activityrow{$\pi_1$}
        \activity{$\tau_1$}{2} 
        \lateactivity{$\tau_3$}{1} 
    
        \activityrow{$\pi_2$} 
        \activity{$\tau_2$}{2} 
        \gevent{MCR}{2}
        \gcomment{0}{0}{2}{
            \small{highest}}{0}
    \end{gantt}
    \end{subfigure}
    \caption{Comments and events}
    \label{comment}
\end{figure*}
\subsection{Auto-formatted activity}

To avoid redundancy, it is possible to use auto-formed task activities. 
The following commands are compatible with the core commands 
\texttt{$\backslash$activity} and \texttt{$\backslash$lateactivity} and
every other commands from the package.

\subsubsection{Auto-formatted commands}

As before, the environment \texttt{$\backslash$gantt} must be used.
The next two commands are the core commands to use for auto-formatted activities.
\newline
The command \texttt{$\backslash$gtaskactivity\{<tasknum>\}\{<length>\}}
display an activity for the task number $<$tasknum$>$ for $<$length$>$ instant.
It uses the special colour and pattern of task $<$tasknum$>$.
\textbf{Prerequisite: $ 0 < $ tasknum $ < 11$.}
\newline
The command \texttt{$\backslash$gjobactivity\{<tasknum>\}\{<jobnum>\}\{<length>\}}
is similar to the previous command, but displaying a job activity including the 
number of the job $<$jobnum$>$.
\textbf{Prerequisite: $ 0 < $ tasknum $ < 11$.}

Fig.~\ref{gtask} and Fig.~\ref{gjob} illustrate the three commands.

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \activityrow{$\pi_1$}
    \gtaskactivity{1}{1}
    \gtaskactivity{2}{1}
    \gtaskactivity{3}{2}
    \lagtime{idle}{1}
    
    \activityrow{$\pi_2$}
    \gtaskactivity{2}{1}
    \gtaskactivity{3}{1}
    \gtaskactivity{4}{1}
    \lagtime{idle}{1}
\end{gantt}
\end{lstlisting}
    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
            \activityrow{$\pi_1$}
            \gtaskactivity{1}{1}
            \gtaskactivity{2}{1}
            \gtaskactivity{3}{2}
        
            \activityrow{$\pi_2$}
            \gtaskactivity{2}{1}
            \gtaskactivity{3}{1}
            \gtaskactivity{4}{1}
        	\lagtime{idle}{1}
        \end{gantt}
    \end{subfigure}
    \caption{Use of \texttt{$\backslash$gtaskactivity}}
    \label{gtask}
\end{figure*}

\begin{figure*}[t]
\centering
\begin{subfigure}[c]{0.5\textwidth}
\centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \activityrow{$\pi_1$}
    \gjobactivity{1}{1}{1}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{2}
    
    \activityrow{$\pi_2$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
    \gjobactivity{4}{1}{1}
    \lagtime{idle}{1}
\end{gantt}
\end{lstlisting}
\end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
            \activityrow{$\pi_1$}
            \gjobactivity{1}{1}{1}
            \gjobactivity{2}{1}{1}
            \gjobactivity{3}{1}{2}
        
            \activityrow{$\pi_2$}
            \gjobactivity{2}{1}{1}
            \gjobactivity{3}{1}{1}
            \gjobactivity{4}{1}{1}
        	\lagtime{idle}{1}
        \end{gantt}
    \end{subfigure}
    \caption{Use of \texttt{$\backslash$gjobactivity}}
    \label{gjob}
\end{figure*}

\subsubsection{Format customisation}

The following commands may be used to customise the format of the core commands.
\begin{itemize}
    \item The command \texttt{$\backslash$enablepattern\{\}} enables the use of patterns
The command \texttt{$\backslash$disablepattern\{\}} disables the use of patterns
Fig.~\ref{gpattern} illustrates the use of these commands:

    \item The command \texttt{$\backslash$gtaskdisplay\{<task\_nb>\}} is used to format the 
task display. $<$task\_nb$>$ is the number of the task.
It may be overwritten to customise task display.
Similarly \texttt{$\backslash$gjobdisplay\{<task\_nb>\}\{<job\_nb>\}} may be 
renewed. 
$<$task\_nb$>$ is the task number. 
$<$job\_nb$>$ is the job number.
This is illustrated in Fig.~\ref{display}. 
\end{itemize}

\begin{figure*}[t]
    \centering
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \disablepattern{}
    \activityrow{$\pi_1$}
    \gjobactivity{1}{1}{1}
    \gjobactivity{2}{1}{1}

    \enablepattern{}
    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
\end{gantt}
\end{lstlisting}

    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
\begin{gantt}{4}{2}{0.8}{0.8}
    \activityrow{$\pi_1$}
    \disablepattern{}
    \gjobactivity{1}{1}{1}
    \gjobactivity{2}{1}{1}

    \enablepattern{}
    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
	\lagtime{idle}{1}
\end{gantt}
    \end{subfigure}
    \caption{Enable or disable patterns}
    \label{gpattern}
\end{figure*}


\begin{figure*}[t]
    \centering
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \renewcommand{\gtaskdisplay}
        [1]{$t^{#1}$}
    \renewcommand{\gjobdisplay}
        [2]{$J^{#1}_{#2}$}
    \activityrow{$\pi_1$}
    \gtaskactivity{1}{1}{1}
    \gtaskactivity{2}{1}{1}
    
    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
    \lagtime{idle}{1}
\end{gantt}
\end{lstlisting}

    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
    \renewcommand{\gtaskdisplay}
        [1]{$t^{#1}$}
    \renewcommand{\gjobdisplay}
        [2]{$J^{#1}_{#2}$}
    \activityrow{$\pi_1$}
    \gtaskactivity{1}{1}{1}
    \gtaskactivity{2}{1}{1}

    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
	\lagtime{idle}{1}
\end{gantt}
    \end{subfigure}
    \caption{Re-formatting task and job's name display}
    \label{display}
\end{figure*}

Also, every colour / pattern is hard-coded, and can be redefined.
For example, the variable defining the $10^{th}$ task's pattern is the following 
\linebreak
\texttt{$\backslash$defsetvar\{rtgpattern10\}\{checkerboard\}}.
The variable defining the $2^{nd}$ task's colour is the following: 
\texttt{$\backslash$defsetvar\{rtgcolour2\}\{Lavender\}}.

\subsubsection{Gray auto-formed tasks}

It is possible to use gray tasks by simply using 
\texttt{$\backslash$disablecolours}.
They may be back by using
\texttt{$\backslash$enablecolours}.
This is depicted in Fig.~\ref{colours}

\begin{figure*}[t]
    \centering
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
\begin{lstlisting}
\begin{gantt}{4}{2}{1}{1}
    \disablecolours{}
    \activityrow{$\pi_1$}
    \gjobactivity{1}{1}{1}
    \gjobactivity{2}{1}{1}

    \enablecolours{}
    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
\end{gantt}
\end{lstlisting}

    \end{subfigure}%
    ~
    \begin{subfigure}[c]{0.5\textwidth}
        \centering
        \begin{gantt}{4}{2}{0.8}{0.8}
    \disablecolours{}
    \activityrow{$\pi_1$}
    \gjobactivity{1}{1}{1}
    \gjobactivity{2}{1}{1}

    \enablecolours{}
    \activityrow{$\pi_1$}
    \gjobactivity{2}{1}{1}
    \gjobactivity{3}{1}{1}
	\lagtime{idle}{1}
\end{gantt}
    \end{subfigure}
    \caption{Enable or disable colours}
    \label{colours}
\end{figure*}

\subsubsection{More than ten tasks?}

It is possible to display more than ten tasks using the auto-formatting activities.
However, it must be done by hand using the command 
\linebreak
\texttt{$\backslash$barshapepattern\{<value>\}\{<length>\}\{<pattern>\}\{<fg>\}\{<bg>\}}
\linebreak
\texttt{\{<opacity>\}}.
\end{document}
